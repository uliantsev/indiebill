from gino.ext.aiohttp import Gino
from gino.declarative import Model


db = Gino()
Base: Model = db.Model

PATRONE_STATUS = ('active_patron', 'declined_patron', 'former_patron')


class Provider(Base):
    __tablename__ = 'providers'

    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(length=30), unique=True)
    client_id = db.Column(db.String)
    client_secret = db.Column(db.String)
    access_token = db.Column(db.String)
    refresh_token = db.Column(db.String)
    redirect_url = db.Column(db.String)

    def __init__(self, **kw):
        super().__init__(**kw)
        self._sessions = set()

    @property
    def sessions(self):
        return self._sessions

    @sessions.setter  # type: ignore
    def add_session(self, session):
        self._sessions.add(session)
        session.provider = self


class Token(Base):
    __tablename__ = 'tokens'

    id = db.Column(db.Integer, primary_key=True, unique=True)
    access_token = db.Column(db.String)
    refresh_token = db.Column(db.String)
    expires_in = db.Column(db.Integer)
    scope = db.Column(db.String)
    token_type = db.Column(db.String)
    version = db.Column(db.String)

    def __init__(self, **kw):
        super().__init__(**kw)
        self._sessions = set()

    @property
    def sessions(self):
        return self._sessions

    @sessions.setter  # type: ignore
    def add_session(self, session):
        self._sessions.add(session)
        session.token = self


class OAuthSession(Base):
    __tablename__ = 'oauth_session'

    id = db.Column(db.Integer, primary_key=True, unique=True)
    provider_id = db.Column(db.ForeignKey('providers.id'))
    token_id = db.Column(db.ForeignKey('tokens.id'))

    def __init__(self, **kw):
        super().__init__(**kw)
        self._users = set()

    @property
    def users(self):
        return self._users

    @users.setter  # type: ignore
    def add_user(self, user):
        self._users.add(user)
        user.oauth = self


class User(Base):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, unique=True)
    session_id = db.Column(db.ForeignKey('oauth_session.id'))
    session_game = db.Column(db.String)
    created_at = db.Column(db.DateTime)
    entered_at = db.Column(db.DateTime)
    email = db.Column(db.String)
    nickname = db.Column(db.String, nullable=True)
    first_name = db.Column(db.String, nullable=True)
    last_name = db.Column(db.String, nullable=True)
    image_url = db.Column(db.String, nullable=True)
    total_donated_cents = db.Column(db.Integer, default=0)
    patron_status = db.Column(db.String, nullable=True)
    patron_id = db.Column(db.Integer, nullable=True, unique=True)

    def is_donated(self):
        return (self.total_donated_cents > 0 or
                self.patron_status in PATRONE_STATUS)
