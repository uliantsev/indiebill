import json
from struct import unpack

from mode import Service

__all__ = (
    'WebServer',
)


class Application:

    def oauth_check(self, session_id=None):
        pass

    async def handler_wrapper(self, reader, writer):

        data = await reader.read(1024)
        # data = await reader.readline()
        s_bits = 4
        size, message = data[:s_bits], data[s_bits:]
        size, *_ = unpack('I', size)
        addr = writer.get_extra_info('peername')

        print(f"Received {message!r} from {addr!r}")
        resp = ''

        user_data = json.dumps(message)
        name_handler = user_data.get('handler')

        if name_handler:
            del user_data['name_handler']

            _handler = self.getattr(self, name_handler)

            if not _handler or not callable(_handler):
                resp = 'Wrong handler'

            resp = _handler(**user_data)

        print(f"Send: {resp!r}")
        writer.write(resp)
        await writer.drain()

        print("Close the connection")
        writer.close()


class TcpServer(Service):
    def __init__(self, host: str, port: int):
        super().__init__()