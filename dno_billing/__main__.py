from os import path

import ssl
import asyncio

from .tcp_server import Application


async def main(host, port):
    ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    if not (path.isfile('crt.pem') and path.isfile('key.pem')):
        raise Exception('Check by contains project\'s chain files for cert')

    ctx.load_cert_chain('crt.pem', 'key.pem')
    app = Application()

    server = await asyncio.start_server(
        app.handler, host, port, ssl=ctx)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()


asyncio.run(main('0.0.0.0', 6660))
