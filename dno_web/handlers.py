from aiohttp.web import json_response, HTTPOk, HTTPError
from loguru import logger
from marshmallow import fields
from webargs.aiohttpparser import AIOHTTPParser

from dno_billing.models import Provider, OAuthSession, Token
from dno_web.oauth import patreon
from dno_web.serializes import UserSchema

parser = AIOHTTPParser()
use_args = parser.use_args
use_kwargs = parser.use_kwargs

CURRENT_APP_ID = '515bb590-6f2b-48b4-967c-846964f80d0b'


async def health_check(request):
    return json_response('Hello web server DNO')


@use_kwargs({'session_game': fields.Str(data_key='state', missing=None),
             'code': fields.Str(data_key='code', missing=None)},
            locations=('query',))
async def patreon_handling(req, session_game=None, code=None):
    if not (session_game or code):
        return json_response('Missing required params', status=400)

    q_prov = Provider.query.where(Provider.name == 'patreon')
    provider = await q_prov.gino.first()
    if not provider:
        logger.warning('Can\'t find providerfor Patreon')
        return HTTPError()

    oauth_client = patreon.OAuth(provider.client_id, provider.client_secret)
    tokens = await oauth_client.get_tokens(code, provider.redirect_url)
    if 'access_token' not in tokens:
        logger.warning(f'Get_tokens: {tokens.get("errors", "no received")}')
        return HTTPError()

    token = Token(**tokens)
    await token.create()

    oauth_session = OAuthSession(token_id=token.id, provider_id=provider.id)
    await oauth_session.create()

    api_client = patreon.API(token.access_token, token.token_type)

    includes = ['memberships']
    fields = {'user': ['email', 'first_name', 'last_name',
                       'full_name', 'last_name', 'image_url'],
              'member': ['patron_status', 'currently_entitled_amount_cents']}
    response = await api_client.get_identity(fields=fields, includes=includes)
    if isinstance(response, dict):
        logger.warning(f'User unauthorized: {response.get("errors", "")}, '
                       f'Session: {session_game}')
        return HTTPError()

    schema = UserSchema()
    schema.context['current_app'] = CURRENT_APP_ID
    user = schema.load(response.json_data['data'])

    user.session_id = oauth_session.id
    user.session_game = session_game
    await user.create()

    return HTTPOk()
