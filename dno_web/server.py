from loguru import logger

from mode import Service
from aiohttp import web

from dno_billing.models import db
from .config import config

__all__ = (
    'WebServer',
)


class WebServer(Service):

    def __init__(self, host: str, port: int):
        super().__init__()
        self.app = web.Application(middlewares=[db])
        cfg = config()
        self.app['config'] = cfg
        self.app['config']['gino'] = {
            'host': cfg.db_host,
            'port': cfg.db_port,
            'user': cfg.db_user,
            'password': cfg.db_password,
            'database': cfg.db_name,
            'kwargs': {
                'max_inactive_connection_lifetime':
                    cfg.max_inactive_connection_lifetime,
            }
        }
        db.init_app(self.app)

        self.host = host
        self.port = port
        self.runner = None
        self.site = None

    async def on_start(self):
        self.runner = web.AppRunner(self.app)
        await self.runner.setup()
        self.site = web.TCPSite(self.runner, self.host, self.port)
        await self.site.start()
        logger.info('Serving on {}:{}', self.host, self.port)

    async def on_stop(self):
        if self.runner is not None:
            await self.runner.cleanup()
