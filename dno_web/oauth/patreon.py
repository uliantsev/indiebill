from urllib.parse import urlencode

import patreon
from aiohttp import ClientSession
from patreon.jsonapi.parser import JSONAPIParser
from patreon.jsonapi.url_util import build_url


async def _request(url, method='GET', **kwargs):
    async with ClientSession() as client:
        request = await client.request(method, url, **kwargs)
        return await request.json()


class API(patreon.API):

    def __init__(self, access_token, token_type='Bearer'):
        super().__init__(access_token)
        self.access_token = access_token
        self.token_type = token_type

    def get_identity(self, includes=None, fields=None):
        url = 'identity'
        return self.__get_jsonapi_doc(
            build_url(url, includes=includes, fields=fields)
        )

    # Internal methods
    async def __get_jsonapi_doc(self, suffix):
        response_json = await self.__get_json(suffix)
        if response_json.get('errors'):
            return response_json
        return JSONAPIParser(response_json)

    async def __get_json(self, suffix):
        response = await _request(
            "https://www.patreon.com/api/oauth2/v2/{}".format(suffix),
            headers={'Authorization': f'{self.token_type} {self.access_token}'}
        )
        if isinstance(response, dict):
            return response
        return response.json()


class OAuth(object):
    def __init__(self, client_id, client_secret):
        super().__init__()
        self.client_id = client_id
        self.client_secret = client_secret

    async def get_tokens(self, code, redirect_uri):
        return await self.__update_token({
            "grant_type": "authorization_code",
            "code": code,
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "redirect_uri": redirect_uri
        })

    async def refresh_token(self, refresh_token, redirect_uri=None):
        return await self.__update_token({
            "grant_type": "refresh_token",
            "refresh_token": refresh_token,
            "client_id": self.client_id,
            "client_secret": self.client_secret
        })

    @staticmethod
    async def __update_token(params):
        response = await _request(
            "https://www.patreon.com/api/oauth2/token",
            method='POST',
            data=urlencode(params),
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        )
        # breakpoint()
        if isinstance(response, dict):
            return response
        return response.json()
