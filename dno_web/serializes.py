from marshmallow import Schema, EXCLUDE, fields, pre_load, post_load

from dno_billing.models import User


class UserSchema(Schema):
    nickname = fields.Str()
    first_name = fields.Str()
    last_name = fields.Str()
    image_url = fields.Str()
    donated = fields.Boolean()
    email = fields.Email()
    type_member = fields.Str()
    total_donated_cents = fields.Integer(required=False)
    patron_status = fields.Str()
    patron_id = fields.Integer()

    class Meta:
        unknown = EXCLUDE

    @pre_load
    def nested_fields(self, data, **_):
        current_app = self.context.get('current_app')

        data.update(data['attributes'])
        data['patron_id'] = data['id']

        # Info about payment statuses
        included = [i for i in data.get('included', []) if current_app in i]

        key_amount_cents = 'currently_entitled_amount_cents'
        if included:
            included = included[0]
            included_attr = included['attributes']
            data['total_donated_cents'] = included_attr[key_amount_cents]
            data['patron_status'] = included_attr['patron_status']

        # Info about memberships
        memberships = data['relationships']['memberships']
        membership = [d for d in memberships.get('data', [])
                      if current_app in d]
        if membership:
            data['type_member'] = membership[0].get('type')
        return data

    @post_load
    def load_to_object(self, data, **_):
        return User(**data)
