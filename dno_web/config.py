import os
import collections
from argparse import ArgumentParser

from addict import Dict


def base_parser():
    parser = ArgumentParser(add_help=False)
    parser.add_argument("--log-level", default="INFO",
                        help="Python logging level [default: %(default)s]")
    parser.add_argument("--web-host", default="0.0.0.0",
                        help="Web server host [default: %(default)s]")
    parser.add_argument("--web-port", default="8080", type=int,
                        help="Web server port [default: %(default)s]")
    parser.add_argument("--db-host", default="localhost", type=str,
                        help="Database server host [default: %(default)s]")
    parser.add_argument("--db-port", default="5432", type=int,
                        help="Database server port [default: %(default)s]")
    parser.add_argument("--db-user", default="dno_agent", type=str,
                        help="Database username [default: %(default)s]")
    parser.add_argument("--db-password", default="", type=str,
                        help="Database password [default: %(default)s]")
    parser.add_argument("--db-name", default="dno", type=str,
                        help="Database name port [default: %(default)s]")
    parser.add_argument("--max-inactive-connection-lifetime", default="60",
                        type=int,
                        help="Close connections on unused pool connections "
                             "[default: %(default)s]")
    return parser


def config():
    """ Define configuration from environment or arguments
    with this order for priority.
    :return: Dict of config
    """
    cm = collections.ChainMap()
    parser = base_parser()
    cm = cm.new_child(vars(parser.parse_args()))

    env = {}
    for k, v in os.environ.items():
        k = k.lower()
        if k not in cm:
            continue
        _type = type(cm[k])
        env[k] = _type(v)
    cm = cm.new_child(env)
    return Dict(**cm)
