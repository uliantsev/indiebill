from mode import Worker

from .server import WebServer
from .handlers import health_check, patreon_handling
from .config import config


def main():
    cfg = config()

    web_server = WebServer(cfg.web_host, cfg.web_port)
    web_server.app.router.add_route("GET", "/healthcheck", health_check)
    web_server.app.router.add_route("GET", "/patreon_handling",
                                    patreon_handling)

    Worker(web_server).execute_from_commandline()


main()
